---
layout: job_page
title: "Support Lead"
---

As the Support Lead, you will take overall responsibility for making sure that GitLab's customers receive an excellent service experience. This involves leading from within, jumping in on tickets when needed but spending the majority of your time determining and implementing the best strategies to measure and improve team performance, and interface with Sales, Customer Success, Development, and the rest of the GitLab team.

The majority of our current customer base is in the Americas, and the Support Lead will be expected to respond to (or coordinate the team to respond to) sometimes unpredictable customer requests. For this reason, we are currently seeking candidates who are committed to working business hours aligned with the Americas time zones. 


## Responsibilities

- Build and lead the team of Service Engineers by actively seeking and hiring globally distributed talent
- Lead team meetings, conduct 1:1's, and continuously improve the onboarding and training process / experience.
- Fully capable and excited to step into the role of a [Service Engineer](https://about.gitlab.com/jobs/service-engineer) when needed.
- Develop and implement data-driven tactics to deliver on the strategic goals for Support, as set out on the overall team's [strategy](https://about.gitlab.com/strategy/) page as well as outlined in the [direction for Support](https://about.gitlab.com/handbook/support/#support-direction).
- Develop and use various analytics to review the Service Engineering team performance - from individual to group - and adjust processes or provide mentorship as needed to improve performance and enjoyment.
- Organize and maintain the [on-call schedule](https://about.gitlab.com/handbook/on-call/) for customer emergencies.
- Construct and monitor metrics to maintain customer and user SLA's, and customer experience.
- Ensure that creation and maintenance of documentation, training materials, and other references are a natural part of the Service Engineering workflow.
- Handle the interfaces between the Support organization and Sales, Development, Customer Success, and the rest of the GitLab team.

## Requirements

- 5 years or more experience in Support in a leadership role
- Vast experience with Zendesk, and its features around Insights, integrations with SalesForce, etc.
- Above average knowledge of Unix and Unix based Operating Systems
- Vast experience with Ruby on Rails Applications and git
- Affinity for (and experience with) providing customer support
- Excellent spoken and written English
- You share our [values](/handbook/#values), and work in accordance with those values
- [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.
- A customer scenario interview is part of the hiring process for this position.
